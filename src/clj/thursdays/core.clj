(ns thursdays.core
  (:gen-class)
  (:require [thursdays.db.migrations :as db-migrations]
            [clojure.tools.logging :as log]
            [thursdays.http.server :as http-server]
            [thursdays.config :as config]))


(defn -main
  [& args]
  "Applies migrations and starts the thursdays webserver"

  (db-migrations/migrate)
  (log/info "All migrations applied")
  (db-migrations/migrate-test)
  (log/info "All migrations applied to test-db")
  (http-server/start  (config/port)))


