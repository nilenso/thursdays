(ns thursdays.models.restaurants
  (:require [thursdays.db.restaurants :as db-restaurants]
            [clojure.spec.alpha :as s]
            [clojure.tools.logging :as log]))

(s/def ::name (s/and string? #(not (clojure.string/blank? %))))
(s/def ::restaurant (s/keys :req-un [::name]))

(defn create-restaurant [restaurant db-spec]
  (db-restaurants/create-restaurant restaurant db-spec))

(defn get-restaurants [db-spec]
  (db-restaurants/get-restaurants db-spec))

