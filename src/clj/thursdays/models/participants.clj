(ns thursdays.models.participants
  (:require [thursdays.db.participants :as db-participants]
            [thursdays.models.hyphen-to-underscore :as hyphen-to-underscore]
            [clojure.spec.alpha :as s]
            [thursdays.config :as config]))

(s/def ::order-id int?)
(s/def ::user-id int?)
(s/def ::participant (s/keys ::req-un [::order-id ::user-id]))


(defn create-participant [body db-spec]
  (let [new-body (hyphen-to-underscore/hyphen-to-underscore-converter body)]
    (db-participants/create-participant new-body db-spec)))


(defn get-participant-order [user-id order-id db-spec]
  (db-participants/get-participant-order user-id order-id db-spec))

