(ns thursdays.models.orders
  (:require [thursdays.db.orders :as db-order]
            [thursdays.models.hyphen-to-underscore :as hyphen-to-underscore]
            [clojure.spec.alpha :as s]
            [thursdays.config :as config]
            [clojure.tools.logging :as log])
  (:import (java.time.format DateTimeFormatter)
           (java.time LocalDateTime)))


(s/def ::name string?)
(s/def ::freeze-time string?)
(s/def ::user-id int?)
(s/def ::creator-id int?)
(s/def ::executed boolean?)
(s/def ::restaurant-id (s/nilable int?))
(s/def ::order-id int?)
(s/def ::create-order (s/keys :req-un [::name ::freeze-time ::creator-id]
                              :opt-un [::executed ::restaurant-id]))
(s/def ::add-restaurant (s/keys :req-un [::restaurant-id ::order-id ::user-id]))
(s/def ::execute-order (s/keys :req-un [::order-id ::user-id ::executed]))

(defn create-order [{:keys [freeze-time] :as order} db-spec]
  (let [order-with-updated-freeze-time (assoc order :freeze-time (. LocalDateTime parse freeze-time DateTimeFormatter/ISO_LOCAL_DATE_TIME))
        order-new (hyphen-to-underscore/hyphen-to-underscore-converter order-with-updated-freeze-time)]
    (db-order/create-order order-new db-spec)))

(defn update-order-restaurant [update db-spec]
  (db-order/update-order-restaurant update db-spec))

(defn update-order-executed [{:keys [order-id user-id] :as update} db-spec]
  (if (= (db-order/get-creator-id order-id db-spec) user-id)
    (db-order/update-order-executed update db-spec)
    (log/info "fails")))

(defn  get-order [order-id db-spec]
  (db-order/get-order  order-id db-spec))


;(create-order {:name "hello woorld"
;               :freeze-time "2020-07-07T06:53:00"
;               :creator-id 1
;               :executed false
;               :restaurant-id 1} (config/test-db-spec))

;(update-order-restaurant {:order-id 24
;               :restaurant-id 22} (config/test-db-spec))
;
;(update-order-executed {:user-id 1
;                        :order-id 1
;                        :executed true} (config/test-db-spec))

