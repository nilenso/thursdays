(ns thursdays.models.menu-items
  (:require [thursdays.db.menu_items :as menu-items]
            [thursdays.models.hyphen-to-underscore :as hyphen-to-underscore]
            [clojure.spec.alpha :as s]))


(s/def ::name string?)
(s/def ::description string?)
(s/def ::price (s/or :int int? :float float?))
(s/def ::restaurant-id int?)
(s/def ::menu-item-id int?)
(s/def ::menu-items (s/keys :req-un [::name ::description ::price ::restaurant-id]))
(s/def ::edit-menu-items (s/keys :opt-un [::name ::description ::price ::restaurant-id]))

(defn create-menu-item [menu-item db-spec]
  (let [new-menu-item (hyphen-to-underscore/hyphen-to-underscore-converter menu-item)]
    (menu-items/create-menu-item new-menu-item db-spec)))

(defn delete-menu-item [menu-item-id db-spec]
  (menu-items/delete-menu-item (Integer/parseInt menu-item-id) db-spec))

(defn edit-menu-item [menu-item menu-item-id db-spec]
  (let [new-menu-item (hyphen-to-underscore/hyphen-to-underscore-converter menu-item)]
    (menu-items/edit-menu-item new-menu-item (Integer/parseInt menu-item-id) db-spec)))

(defn get-menu-items [restaurant-id db-spec]
  (menu-items/get-menu-items  restaurant-id db-spec))
