(ns thursdays.models.users
  (:require [thursdays.db.users :as db-user]))

(defn create-user [user]
  (db-user/create-user user))

(defn is-admin? [user-id db-spec]
  (db-user/is-admin? user-id db-spec))