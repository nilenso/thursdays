(ns thursdays.models.order-items
  (:require [thursdays.db.order-items :as order-items]
            [thursdays.db.orders :as orders]
            [clojure.tools.logging :as log]
            [thursdays.models.hyphen-to-underscore :as hyphen-to-underscore]
            [clojure.spec.alpha :as s])
  (:import [java.time LocalDateTime]))


(s/def ::menuitem-id int?)
(s/def ::quantity int?)
(s/def ::ordered-by int?)
(s/def ::ordered-for (s/nilable int?))
(s/def ::order-id int?)
(s/def ::order-item (s/keys :req-un [::menuitem-id ::quantity ::ordered-by ::order-id]
                            :opt-un [::ordered-for]))


(defn freeze-time-expired? [order-id db-spec]
  (let [freeze-time (orders/get-freeze-time-as-LocalDateTime order-id db-spec)]
    (. (LocalDateTime/now) isAfter freeze-time)))


(defn is-user-the-creator? [order-id user-id db-spec]
  (if (= (orders/get-creator-id order-id db-spec) user-id)
    true
    false))

(defn is-order-executed? [order-id db-spec]
  (if (= (orders/get-order-status order-id db-spec) true)
    true
    false))

(defn create-order-item [{:keys [order-id ordered-by] :as order-item} db-spec]
  (let [new-order-item (hyphen-to-underscore/hyphen-to-underscore-converter order-item)]
    (if-not (is-order-executed? order-id db-spec)
      (if (freeze-time-expired? order-id db-spec)
        (if (is-user-the-creator? order-id ordered-by db-spec)
          (order-items/create-order-item new-order-item db-spec)
          '())
        (order-items/create-order-item new-order-item db-spec))
      '())))

(defn exists? [order-item db-spec]
  (not (empty? (order-items/exists? order-item db-spec))))

(defn update [{:keys [order-id ordered-by] :as order-item} db-spec]
  (if-not (is-order-executed? order-id db-spec)
    (if (freeze-time-expired? order-id db-spec)
      (if (is-user-the-creator? order-id ordered-by db-spec)
        (order-items/update! order-item db-spec)
        '())
      (order-items/update! order-item db-spec))
    '()))

;(defn- create-order-item-with-freeze-time-check [order-item]
;  (let [create-order-item #(first (order_items/create-order-item order-item))]
;    (if-not (freeze-time-expired? order-id)
;      (create-order-item)
;      (when (is-user-the-creator? order-id ordered-by)
;        (create-order-item)))))
;
;(defn create-order-item [{:keys [order-id ordered-by] :as order-id}]
;  (when-not (is-order-executed? order-id)
;    (create-order-item-with-freeze-time-check order_items)))

;(create-order-item {:menuitem-id 1 :quantity 1 :ordered-by 1 :ordered_for nil :order-id 7})
(defn get-all [order-id]
  (order-items/get-all order-id))

(defn index-by-user-id [order-items]
  (group-by :ordered_by order-items))
