(ns thursdays.http.wrap-user
  (:require [thursdays.http.user-info :refer [get-user-info]]
            [honeysql.core :as sql]
            [honeysql.helpers :refer [select from where]]
            [clojure.java.jdbc :as jdbc]
            [thursdays.config :as config]
            [ring.util.response :refer [bad-request]]))


(defn- fetch-user-details [user-email]
  (jdbc/query (config/db-spec) (-> (select :*)
                                   (from :users)
                                   (where [:= :email user-email])
                                   sql/format)))



(defn wrap-user
  [handler profile]
  (fn [request]
    (if (-> request :oauth2/access-tokens profile)
      (->> (-> request
               (get-user-info)
               (get "email")
               (fetch-user-details)
               (first))
           (assoc request :userInfo)
           (handler))
      (bad-request "user not signed in"))))


