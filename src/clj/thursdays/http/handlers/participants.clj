(ns thursdays.http.handlers.participants
  (:require [clojure.tools.logging :as log]
            [thursdays.models.participants :as models]
            [ring.util.response :refer [response status]]
            [clojure.spec.alpha :as s]
            [thursdays.config :as config]))


(defn validate-and-respond [request-body]
  (log/info "in validate")
  (if (s/valid? ::models/participant request-body)
    (let [response-map (models/create-participant request-body (config/db-spec))]
      (if (= nil (first response-map))
        (status {} 500)
        (response response-map)))
    (log/info s/explain-str ::models/participant request-body)))

(defn create-participant-handler [req]
  (let [order-id (Integer/parseInt (get-in req [:route-params :id]))
        user-id (-> req :userInfo :id)
        modified-request-body (assoc {} :order-id order-id :user-id user-id)]
    (validate-and-respond modified-request-body)))