(ns thursdays.http.handlers.restaurants
  (:require [thursdays.models.restaurants :as models]
            [ring.util.response :refer [resource-response response bad-request status content-type]]
            [clojure.tools.logging :as log]
            [clojure.spec.alpha :as s]
            [aero.core :refer [read-config]]
            [thursdays.config :as config]
            [thursdays.models.users :as user-model]))



(defn create-restaurant-handler [req]
  (log/info "In http handler")
  (let [request-body (:body req)
        testing-header (get (:headers req) "testing")
        user-id (-> req :userInfo :id)]
    (if (user-model/is-admin? user-id (config/db-spec))
      (if (s/valid? ::models/restaurant request-body)
        (response (models/create-restaurant request-body (config/get-db-spec testing-header)))
        (do (log/info (s/explain-str ::models/restaurant request-body))
            (status {} 500)))
      (do (log/info "admin" (user-model/is-admin? user-id (config/db-spec)) "user-id" user-id)
        (status {} 403)))))


(defn index-handler [_]
  (-> (resource-response "index.html" {:root "/cljs/public"})
      (content-type "text/html")))

(defn get-restaurants-handler [req]
  (let [db-spec (config/get-db-spec req)]
    (response (models/get-restaurants db-spec))))

