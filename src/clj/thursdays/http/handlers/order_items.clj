(ns thursdays.http.handlers.order_items
  (:require [thursdays.models.order-items :as models]
            [ring.util.response :refer [response status]]
            [clojure.tools.logging :as log]
            [clojure.spec.alpha :as s]
            [thursdays.config :as config]))


(defn validate-and-respond [request-body]
  (if (s/valid? ::models/order-item request-body)
    (let [response-map (if (models/exists? request-body (config/db-spec))
                         (models/update request-body (config/db-spec))
                         (models/create-order-item request-body (config/db-spec)))]
      (if (= nil (first response-map))
        (status {} 500)
        (response response-map)))))


(defn create-order-item-handler [req]
  (let [order-id (Integer/parseInt (get-in req [:route-params :id]))
        user-id (-> req :userInfo :id)
        modified-request-body (merge {:order-id    order-id
                                      :ordered-by  user-id
                                      :ordered-for nil}
                                     (:body req))]
    (validate-and-respond modified-request-body)))

