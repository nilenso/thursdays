(ns thursdays.http.handlers.oauth
  (:require [ring.middleware.oauth2 :refer [wrap-oauth2]]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.util.response :as res]
            [thursdays.models.users :as models]
            [thursdays.http.user-info :refer [get-user-info]]))


(defn signed-in [req]
  (-> req
      (get-user-info)
      (select-keys ["name" "email"])
      (models/create-user))
  (res/redirect "/"))

(defn signed-in-2 [req]
  (-> (res/response (str "Hello " (-> req :userInfo :name)))
      (res/content-type "text/plain")))

