(ns thursdays.http.handlers.menu_items
  (:require [thursdays.models.menu-items :as models]
            [ring.util.response :refer [response status]]
            [clojure.tools.logging :as log]
            [clojure.spec.alpha :as s]
            [thursdays.config :as config]
            [thursdays.models.users :as user-model]))

(defn get-testing-header-and-get-db-spec [req]
  (config/db-spec))

(defn validate-request-and-respond
  ([spec request-param-id request-body db-spec fun]
   (if (s/valid? spec request-body)
     (response (fun request-body request-param-id db-spec))
     (do (log/info (s/explain-str spec request-body))
         (status {} 500))))

  ([spec request-body db-spec fun]
   (if (s/valid? spec request-body)
     (response (fun request-body db-spec))
     (do (log/info (s/explain-str spec request-body))
         (status {} 500)))))

(defn create-menu-item-handler [req]
  (let [db-spec (get-testing-header-and-get-db-spec req)
        user-id (-> req :userInfo :id)]
    (if (user-model/is-admin? user-id (config/db-spec))
      (validate-request-and-respond ::models/menu-items (:body req) db-spec models/create-menu-item)
      (status {} 403))))

(defn delete-menu-item-handler [req]
  (let [menu-item-id (get-in req [:route-params :id])
        db-spec (get-testing-header-and-get-db-spec req)
        user-id (-> req :userInfo :id)]
    (if (user-model/is-admin? user-id (config/db-spec))
      (if (s/valid? ::models/menu-item-id (Integer/parseInt menu-item-id))
        (response (models/delete-menu-item menu-item-id db-spec))
        (status {} 500))
      (status {} 403))))

(defn edit-menu-item-handler [req]
  (let [user-id (-> req :userInfo :id)]
    (if (user-model/is-admin? user-id (config/db-spec))
      (validate-request-and-respond ::models/edit-menu-items (get-in req [:route-params :id]) (:body req) (config/db-spec) models/edit-menu-item)
      (status {} 403))))

(defn get-menu-item-handler [req]
  (let [db-spec (get-testing-header-and-get-db-spec req)]
    (response (models/get-menu-items (Integer/parseInt (get-in req [:route-params :id])) db-spec))))
