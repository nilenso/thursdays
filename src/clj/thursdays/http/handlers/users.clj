(ns thursdays.http.handlers.users)

(defn get-user-handler [req]
  (ring.util.response/response (:userInfo req)))
