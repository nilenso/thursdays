(ns thursdays.http.handlers.orders
  (:require [clojure.tools.logging :as log]
            [thursdays.models.orders :as orders]
            [thursdays.models.participants :as participants]
            [ring.util.response :refer [response bad-request status]]
            [clojure.spec.alpha :as s]
            [thursdays.config :as config]
            [thursdays.models.order-items :as order-items]))



(defn validate-request-and-respond [spec request-body function]
  (if (s/valid? spec request-body)
    (let [content (function request-body (config/db-spec))]
      (if (not-empty content)                               ;return 500 on error else the updated entry
        (response content)
        (do
          (log/error "Empty content")
          (status {} 500))))
    (do
      (log/error "Invalid content" (s/explain-str spec request-body))
      (status {} 500))))


(defn create-order-handler [req]
  (let [creator-id (-> req :userInfo :id)
        modified-request-body (assoc (:body req) :creator-id creator-id)]
    (validate-request-and-respond ::orders/create-order modified-request-body orders/create-order)))

(defn restaurant-update-handler [req]
  (let [order-id (Integer/parseInt (get-in req [:route-params :id]))
        user-id (-> req :userInfo :id)
        modified-request-body (assoc (:body req) :order-id order-id :user-id user-id)]
    (validate-request-and-respond ::orders/add-restaurant modified-request-body orders/update-order-restaurant)))


(defn executed-update-handler [req]
  (let [order-id (Integer/parseInt (get-in req [:route-params :id]))
        user-id (-> req :userInfo :id)
        modified-request-body (assoc (:body req) :order-id order-id :user-id user-id)]
    (validate-request-and-respond ::orders/execute-order modified-request-body orders/update-order-executed)))


(defn get-order-handler [req]
  (let [order-id (Integer/parseInt (get-in req [:route-params :id]))
        user-id (get-in req [:userInfo :id])
        order (orders/get-order order-id (config/db-spec))
        participating (not (nil? (participants/get-participant-order user-id order-id (config/db-spec))))
        items (-> (order-items/get-all order-id)
                  (order-items/index-by-user-id))]

    (response {:freeze-time   (:freeze_time order)
               :name          (:name order)
               :id            order-id
               :participating participating
               :order-items   items
               :restaurant-id (:restaurant_id order)
               :creator-id    (:creator_id order)})))






