(ns thursdays.http.user-info
  (:require [clj-http.client :as http]
            [cheshire.core :refer [parse-string]]))


(defn- retrieve-access-token
  [req]
  (let [tokens (-> req :oauth2/access-tokens :google)]
    tokens))


(defn get-user-info
  [req]
  (let [access-token (-> req retrieve-access-token :token)]
    (let [url "https://openidconnect.googleapis.com/v1/userinfo"
          response (http/get url {:oauth-token      access-token
                                  :throw-exceptions false})]
      (if (http/success? response)
        (-> response
            :body
            parse-string)))))