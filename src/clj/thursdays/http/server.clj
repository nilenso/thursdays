(ns thursdays.http.server
  (:require [ring.adapter.jetty :as jetty]
            [ring.middleware.params :refer [wrap-params]]
            [compojure.core :refer [defroutes GET POST ANY]]
            [compojure.route :as route]
            [thursdays.http.handlers.oauth :as oauth]
    ;[ring.middleware.oauth2 :refer [wrap-oauth2]]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults api-defaults]]

            [thursdays.http.oauth_mid :refer [wrap-oauth2]]

            [compojure.core :refer [defroutes GET POST PUT DELETE ANY]]
            [thursdays.http.handlers.orders :as order]
            [thursdays.http.handlers.participants :as participants]
            [thursdays.http.handlers.menu_items :as menu-items]
            [thursdays.http.handlers.order_items :as order-items]
            [ring.middleware.json :refer [wrap-json-body wrap-json-response]]
            [thursdays.http.handlers.restaurants :as restaurants]
            [thursdays.http.handlers.users :as users]
            [thursdays.http.wrap-user :refer [wrap-user]]
            [ring.middleware.resource :as resource]
            [clojure.tools.logging :as log]
            [ring.util.response :as response]))

(defn wrap [handler]
  (-> handler
      (wrap-json-body {:keywords? true})
      (wrap-json-response)))

(defn- add-redirect-uri-fns-to-profiles [profiles]
  (->> profiles
       (map (fn [[k v]] [k (assoc v :redirect-uri-fn (constantly (thursdays.config/oauth2-redirect-uri)))]))
       (into {})))

(defroutes auth-routes
           (GET "/signedin" [] oauth/signed-in)
           (GET "/foo" [] oauth/signed-in-2)
           (POST "/api/orders" [] (wrap order/create-order-handler))
           (GET "/api/orders/:id" [] (wrap order/get-order-handler))
           (GET "/api/user" [] (wrap users/get-user-handler))
           (POST "/api/orders/:id/participant" [] (wrap participants/create-participant-handler))
           (POST "/api/orders/:id/restaurant" [] (wrap order/restaurant-update-handler))
           (POST "/api/orders/:id/items" [] (wrap order-items/create-order-item-handler))
           (POST "/api/orders/:id/executed" [] (wrap order/executed-update-handler))
           (POST "/api/restaurants" [] (wrap restaurants/create-restaurant-handler))
           (POST "/api/menu-items" [] (wrap menu-items/create-menu-item-handler))
           (DELETE "/api/menu-items/:id" [] menu-items/delete-menu-item-handler)
           (PUT "/api/menu-items/:id" [] (wrap menu-items/edit-menu-item-handler))

           (route/not-found (-> restaurants/index-handler
                                ((fn [handler]
                                   (fn [req]
                                     (-> (handler req)
                                         (response/status 200))))))))


(defroutes non-auth-routes
           (GET "/" [] restaurants/index-handler)
           (GET "/api/restaurants" [] (wrap restaurants/get-restaurants-handler))
           (GET "/api/menu-items/:id" [] (wrap menu-items/get-menu-item-handler)))

(defroutes app
           (ANY "*" [] non-auth-routes)
           (ANY "*" [] (let [oauth2-profiles (add-redirect-uri-fns-to-profiles (thursdays.config/oauth2-spec))]
                         (-> auth-routes
                             (wrap-user :google)
                             (wrap-oauth2 oauth2-profiles)))))


(defn int-parse [port]
  (try
    (Integer/parseInt port)
    (catch Exception e (log/info e) 8080)))
(defonce server (atom nil))

(defn start [port]
  (reset! server (jetty/run-jetty (-> app
                                      (wrap-oauth2 (thursdays.config/oauth2-spec))
                                      (wrap-defaults (-> api-defaults (assoc-in [:session :cookie-attrs :same-site] :lax)))
                                      (ring.middleware.params/wrap-params)
                                      (resource/wrap-resource "cljs/public")) {:port  (int-parse port)
                                                                               :join? false})))

(defn stop []
  (.stop @server)
  (reset! server nil))


