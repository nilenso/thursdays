(ns thursdays.db.order-items
  (:require [thursdays.config :as config]
            [honeysql.core :as sql]
            [honeysql.helpers :as helpers :refer [select from where sset]]
            [clojure.java.jdbc :as jdbc]
            [honeysql-postgres.helpers :as psqlh]
            [honeysql.helpers :refer [select from where join]]
            [clojure.java.jdbc :as jdbc]))

(defn create-order-item [order-item db-spec]
  (jdbc/insert! db-spec :order_items order-item))

(defn update! [{:keys [quantity ordered-by order-id menuitem-id]} db-spec]
  (jdbc/execute! db-spec (-> (helpers/update :order_items)
                             (sset {:quantity quantity})
                             (where [:and [:and [:= :ordered_by ordered-by]
                                           [:= :order_id order-id]]
                                     [:= :menuitem_id menuitem-id]])
                             (psqlh/returning :*)
                             sql/format)))

(defn exists? [{:keys [menuitem-id order-id ordered-by]} db-spec]
  (jdbc/query db-spec (-> (select :id)
                          (from :order_items)
                          (where [:and [:and [:= :order_id order-id]
                                        [:= :menuitem_id menuitem-id]]
                                  [:= :ordered_by ordered-by]])
                          sql/format)))
(defn get-all
  ([order-id db-spec]
   (jdbc/query db-spec (-> (select :*)
                           (from :order_items)
                           (where [:= :order_id order-id])
                           (join :menu_items [:= :menu_items.id :order_items.menuitem_id])
                           (sql/format))))
  ([order-id]
   (get-all order-id (config/db-spec))))
