(ns thursdays.db.users
  (:require [thursdays.config :as config]
            [clojure.java.jdbc :as jdbc]
            [clojure.tools.logging :as log]
            [honeysql.core :as sql]
            [honeysql.helpers :refer [select from where]]))

(defn create-user
  ([user]
   (try
     (jdbc/insert! (config/db-spec) :USERS user)
     (catch Exception e user)))
  ([user db-spec]
   (try
     (jdbc/insert! db-spec :USERS user)
     (catch Exception e (.getMessage e)))))

(defn is-admin? [user-id db-spec]
  (:admin (first (jdbc/query db-spec (-> (select :admin)
                                         (from :users)
                                         (where [:= :id user-id])
                                         sql/format)))))
