(ns thursdays.db.restaurants
  (:require [clojure.java.jdbc :as jdbc]
            [thursdays.config :as config]
            [honeysql.core :as sql]
            [honeysql.helpers :refer [select from where]]
            [clojure.tools.logging :as log]))

(defn create-restaurant [name db-spec]
  (jdbc/insert! db-spec :restaurants name))

(defn get-restaurants [db-spec]
  (jdbc/query db-spec (-> (select :*)
                          (from  :restaurants)
                          sql/format)))
(defn get-restaurant [restaurant-id db-spec]
  (jdbc/query db-spec (-> (select :*)
                          (from :restaurants)
                          (where [:= :id restaurant-id])
                          sql/format)))