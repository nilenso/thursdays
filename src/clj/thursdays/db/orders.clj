(ns thursdays.db.orders
  (:require [thursdays.config :as config]
            [clojure.java.jdbc :as jdbc]
            [honeysql.core :as sql]
            [clojure.tools.logging :as log]
            [honeysql.helpers :refer [sset where select values from insert-into] :as helpers])
  (:import [java.time LocalDateTime]
           (java.sql Timestamp)
           (java.time.format DateTimeFormatter)))


(defn create-order [order db-spec]
  (jdbc/insert! db-spec :orders order))


(defn update-order-restaurant [{:keys [order-id restaurant-id] :as update} db-spec]
  (try (jdbc/execute! db-spec (-> (helpers/update :orders)
                                  (sset {:restaurant_id restaurant-id})
                                  (where [:= :id order-id])
                                  sql/format))
       (catch Exception e (.getMessage e)))
  (jdbc/query db-spec (-> (select :*)
                          (from :orders)
                          (where [:= :id order-id])
                          sql/format)))

(defn update-order-executed [{:keys [order-id executed]} db-spec]
  (try (jdbc/execute! db-spec (-> (helpers/update :orders)
                                  (sset {:executed executed})
                                  (where [:= :id order-id])
                                  sql/format))
       (catch Exception e (.getMessage e)))
  (jdbc/query db-spec (-> (select :*)
                          (from :orders)
                          (where [:= :id order-id])
                          sql/format)))

(defn get-order-status [order-id db-spec]
  (:executed (first (jdbc/query db-spec (-> (select :executed)
                                            (from :orders)
                                            (where [:= :id order-id])
                                            sql/format)))))


(defn get-freeze-time-as-LocalDateTime [order-id db-spec]
  "Given order_id retrieve and convert freeze-time to LocalDateTime"
  (. (:freeze_time (first (jdbc/query db-spec (-> (select :*)
                                                  (from :orders)
                                                  (where [:= :id order-id])
                                                  sql/format)))) toLocalDateTime))


(defn get-creator-id [order-id db-spec]
  (:creator_id (first (jdbc/query db-spec (-> (select :creator_id)
                                              (from :orders)
                                              (where [:= :id order-id])
                                              sql/format)))))


(defn get-order [order-id db-spec]
  (first (jdbc/query db-spec (-> (select :*)
                                 (from :orders)
                                 (where [:= :id order-id])
                                 sql/format))))