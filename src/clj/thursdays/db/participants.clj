(ns thursdays.db.participants
  (:require [thursdays.config :as config]
            [clojure.java.jdbc :as jdbc]
           [clojure.tools.logging :as log]
            [honeysql.core :as sql]
            [honeysql.helpers :refer [sset where select values from insert-into] :as helpers]))

(defn create-participant [participant db-spec]
    (jdbc/insert! db-spec :participants participant))


(defn get-participant-order [user-id order-id db-spec]
  (first (jdbc/query db-spec (-> (select :*)
                                 (from :participants)
                                 (where [:and
                                         [:= :user_id user-id]
                                         [:= :order_id order-id]])
                                 sql/format))))