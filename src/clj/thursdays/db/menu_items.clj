(ns thursdays.db.menu_items
  (:require [thursdays.config :as config]
            [honeysql.core :as sql]
            [honeysql.helpers :refer [select from where sset]]
            [clojure.java.jdbc :as jdbc]))


(defn create-menu-item [menu-item db-spec]
  (jdbc/insert! db-spec :menu_items menu-item))

(defn delete-menu-item [menu-item-id db-spec]
  (jdbc/delete! db-spec :menu_items ["id = ?" menu-item-id]))


(defn edit-menu-item [menu-item menu-item-id db-spec]
  (jdbc/update! db-spec :menu_items menu-item ["id = ?" menu-item-id])
  (jdbc/query db-spec (-> (select :*)
                          (from :menu_items)
                          (where [:= :id menu-item-id])
                          sql/format)))

;(defn get-menu-items [id]
;  (jdbc/query (config/db-spec) (-> (select :*)
;                                   (from :menu_items)
;                                   (where [:= :restaurant_id id])
;                                   sql/format)))


(defn get-menu-items [restaurant-id db-spec]
  (jdbc/query db-spec (-> (select :*)
                          (from :menu_items)
                          (where [:= :restaurant_id restaurant-id])
                          sql/format)))

(defn get-menu-items-menu-id [menu-item-id db-spec]
  (first(jdbc/query db-spec (-> (select :*)
                           (from :menu_items)
                           (where [:= :id menu-item-id])
                           sql/format))))