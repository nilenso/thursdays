(ns thursdaysUI.pages.menu-items
  (:require [thursdaysUI.api.menu-items :as api]
            [thursdaysUI.store :as store]
            [reagent.core :as r]
            [thursdaysUI.underscore-converter :refer [underscore-to-hyphen-converter]]))

(enable-console-print!)

(defn create-or-edit-menu-item
  ([restaurant-id] (create-or-edit-menu-item restaurant-id nil))
  ([restaurant-id menu-item]
   (let [form-state (r/atom (or menu-item
                                {:name          ""
                                 :description   ""
                                 :price         ""
                                 :restaurant-id (js/parseInt restaurant-id)}))]
     (fn []
       (let [{:keys [id name description price]} @form-state]
         ^{:key id}
         [:<>
          [:input {:type     "text"
                   :placeholder "Name"
                   :name     "name"
                   :id       "enter-item-name"
                   :value    name
                   :onChange #(swap! form-state assoc :name (-> % .-target .-value))}]
          [:input {:type     "text"
                   :placeholder "Description"
                   :name     "desc"
                   :id       "enter-item-desc"
                   :value    description
                   :onChange #(swap! form-state assoc :description (-> % .-target .-value))}]
          [:input {:type     "number"
                   :placeholder "Price"
                   :name     "price"
                   :id       "enter-item-price"
                   :value    price :step ".01"
                   :onChange #(swap! form-state assoc :price (-> % .-target .-value js/parseFloat))}]
          [:button
           {:on-click (fn [_event]
                        (if id
                          (api/edit-menu-item @form-state)
                          (api/create-menu-item @form-state)))}
           (if id "Save" "Create")]
          [:span]])))))


(defn- menu-item [{:keys [id name description price restaurant-id] :as menu-item}]
  (let [edit-mode (r/atom false)]
    (fn []
      (if @edit-mode
        ^{:key id} [:div {:class "grid-span-row"}
                    [(create-or-edit-menu-item restaurant-id menu-item)]]
        ^{:key id} [:<>
                    [:span name]
                    [:span description]
                    [:span price]
                    [:button {:on-click (fn [_] (swap! edit-mode not))} "Edit"]
                    [:button {:on-click #(api/delete-menu-item menu-item)} "Delete"]]))))

(defn menu-list [{:keys [id name]}]
  (api/get-menu-items id)
  [:div {:class "center"}
   [:div {:class "bigger-text"} name]
   [:div {:class "menu-list"}
    [:<>
     [:span {:class "bold"} "Name"]
     [:span {:class "bold"} "Description"]
     [:span {:class "bold"} "Price"]
     [:span] [:span]
     (map (fn [{:keys [id] :as item}]
                ^{:key id} [(menu-item item)])
          @(store/menu-state))
     [(create-or-edit-menu-item id)]]]])