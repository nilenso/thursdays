(ns thursdaysUI.pages.show-order
  (:require [thursdaysUI.pages.base :refer [base]]
            [thursdaysUI.api.order :as order]
            [thursdaysUI.store :as store]
            [thursdaysUI.api.restaurant-list :as restaurants-api]
            [thursdaysUI.api.user :as user-api]
            [thursdaysUI.api.menu-items :as menu-items-api]))

(defn- order-table-rows []
  [:tr
   [:th "Quantity"]
   [:th "Name"]
   [:th "Description"]
   [:th "Price"]])

(defn- all-orders-item-component [{:keys [quantity name description price] :as item}]
  ^{:key (str (gensym "random-key"))} [:tr
                                       [:td quantity]
                                       [:td name]
                                       [:td description]
                                       [:td price]])

(defn- order-summary-component [items]
  [:div
   [:table
    [:thead (order-table-rows)]
    [:tbody (map (fn [item]
                   (map #(all-orders-item-component %) item)) (vals items))]]])

(defn- item-quantity-component [id selections value]
  (if (= value (get @selections id))
    [:option {:value value :selected "true"} value]
    [:option {:value value} value]))

(defn- menu-item-component [selections {:keys [id name description price] :as order-item}]
  ^{:key (str (gensym "random-key"))}
  [:tr
   [:select {:on-change (fn [e]
                          (let [quantity (.parseInt js/window (.-value (.-target e)))]
                            (if (> quantity 0)
                              (swap! selections assoc id quantity)
                              (swap! selections dissoc id))))}
    (map #(item-quantity-component id selections %) (range 10))]
   [:td name]
   [:td description]
   [:td price]])

(defn- menu-list [{:keys [id order-items] :as _order}]
  (let [selections (atom {})]
    [:div ((fn []
             (let [id (.parseInt js/window id)
                   {:keys [restaurant-id] :as order} @(store/order-state id)
                   user-id (:id @(store/user-state))]
               (menu-items-api/fetch-menu-items restaurant-id)
               (reduce #(swap! selections conj %1 %2) (map #(apply hash-map (vals (select-keys % [:menuitem_id :quantity]))) ((keyword (str user-id)) order-items)))
               [:div
                [:table
                 [:thead (order-table-rows)]
                 [:tbody (map (partial menu-item-component selections)
                              (let [x @(store/menu-items-state restaurant-id)] x))]]
                [:button {:disabled (not @(store/participation-state id)) :on-click (fn [_]
                                                                                      (doall (map #(order/add-order-item id %) @selections)))} "Confirm"]])))]))

(defn- restaurant-dropdown [{:keys [name id]}]
  [:option {:value id} name])

(defn- participate-button [id participating]
  (fn []
    (if participating
      [:div "You are participating"]
      [:button {:on-click (fn [_] (order/participate id))} "Participate"])))

(defn- select-restaurant [{:keys [id restaurant-id]} creator-id]
  (let [selected (atom nil)]
    (fn []
      (if restaurant-id
        [:div (str "Restaurant name: " (-> restaurant-id store/get-restaurant :name))]
        (when (= creator-id (:id @(store/user-state)))
          [:div
           [:select {:on-change #(reset! selected (-> % .-target .-value))}
            [:option {:defaultValue "--"
                      :disabled     "false"
                      :hidden       "false"}
             "Select restaurant"]
            (map restaurant-dropdown (vals @store/restaurants-state))]
           [:button {:on-click (fn [_]
                                 (order/update-order-restaurant id
                                                                (->> @selected
                                                                     (.parseInt js/window)))
                                 (order/get id))}
            "Confirm"]])))))

(defn- show-order-component [id]
  (order/get id)
  (user-api/get)
  (restaurants-api/update-list)
  (fn []
    (let [id (.parseInt js/window id)
          {:keys [name freeze-time executed participating creator-id restaurant-id order-items] :as order} @(store/order-state id)]
      (when order
        [:div {:class "center"}
         [:div "Order name: " name]
         [:div "Freeze time: " freeze-time]
         [:div "Order status: " (if executed "ENDED" "LIVE")]
         [(participate-button id participating)]
         (if restaurant-id
           [:div
            (menu-list order)
            (order-summary-component order-items)]
           (if (= creator-id (:id @(store/user-state)))
             [(select-restaurant order creator-id)]
             [:div "Restaurant has not yet been set"]))]))))

(defn show-order [{:keys [id]}]
  [(show-order-component id)])
