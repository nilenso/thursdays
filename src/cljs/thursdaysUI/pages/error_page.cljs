(ns thursdaysUI.pages.error-page)

(defn display-error
  []
  [:div {:class "display-error"}
   [:h1 "Oops! The page you requested could not be found! :P"]
   [:h2 [:a {:href "/"} "Go back to Home page"]]])