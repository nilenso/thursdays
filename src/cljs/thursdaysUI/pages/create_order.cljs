(ns thursdaysUI.pages.create_order
  (:require [thursdaysUI.api :as api]
            [reagent.core :as r]))

(defn create-order []
  (let [order-details (r/atom {:freeze-time nil
                               :name        nil})]
    (fn []
      (let [{:keys [name freeze-time]} @order-details]
        [:div {:class "center"}
         [:input {:type      "text"
                  :name      "order_name"
                  :value     name
                  :on-change (fn [e]
                               (swap! order-details #(assoc % :name (.-value (.-target e)))))}]
         [:input {:type      "datetime-local"
                  :name      "freeze-time"
                  :on-change (fn [e]
                               (swap! order-details #(assoc % :freeze-time (.-value (.-target e)))))}]
         [:button {:on-click (fn []
                               (api/create-order name freeze-time))} "Submit"]]))))