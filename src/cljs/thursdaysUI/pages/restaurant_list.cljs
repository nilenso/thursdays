(ns thursdaysUI.pages.restaurant-list
  (:require [reagent.core :as r]
            [ajax.core :refer [GET POST]]
            [thursdaysUI.api.user :as user]
            [thursdaysUI.api.restaurant-list :as restaurant-api]
            [thursdaysUI.pages.base :refer [base]]
            [thursdaysUI.store :as store]))

(enable-console-print!)

(defn- restaurant-item [{:keys [name id]}]
  ^{:key id}
  [:li {:class "restaurant-name"}
   [:a {:href (str "/menu-items/" id "/" name)} name]])


(defn add-restaurant-form []
  (let [name-field (r/atom "")]
    (fn []
      [:div {:class "restaurant-input"} "Add restaurant: "
       [:input {:type "text" :value @name-field :id "enter-restaurant" :onChange #(reset! name-field (-> % .-target .-value))}]
       [:button {:on-click #(restaurant-api/create-restaurant @name-field)}
        "Submit"]])))

(defn restaurant-list [_params]
  (restaurant-api/update-list)
  [:div {:class "center"}
   [:ul
    [(add-restaurant-form)]
    [:div {:class "restaurant-list"}
     (map restaurant-item (vals @store/restaurants-state))]]])

