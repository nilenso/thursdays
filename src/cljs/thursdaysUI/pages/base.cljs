(ns thursdaysUI.pages.base)

(defn- sidebar []
  [:a {:href "/create-order"} "Create order"])

(defn base [component]
  [:div {:class "center"}
   component
   [sidebar]])
