(ns thursdaysUI.core
  (:require [reagent.core :as r]
            [thursdaysUI.store :as store]
            [thursdaysUI.router :as router]
            [thursdaysUI.pages.create_order :refer [create-order]]
            [thursdaysUI.pages.show-order :refer [show-order]]
            [thursdaysUI.pages.restaurant-list :refer [restaurant-list]]
            [thursdaysUI.pages.menu-items :refer [menu-list]]
            [thursdaysUI.pages.home :refer [home]]
            [thursdaysUI.pages.error-page :refer [display-error]]))

(def pages
  {:home            home
   :restaurant-list restaurant-list
   :restaurant      menu-list
   :create-order    create-order
   :show-order    show-order
   :not-found       display-error})

(defn view []
  (let [active-page (get @store/state :active-page)
        params (get @store/state :params)]
    [(get pages active-page) params]))

(defn ^:export run []
  (store/init)
  (router/init)
  (r/render [view]
            (.getElementById js/document "thursdaysUI")))
