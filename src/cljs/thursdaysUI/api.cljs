(ns thursdaysUI.api
  (:require [ajax.core :refer [GET POST PUT DELETE]]))

(def ^:private create-order-route "/api/orders")
(def ^:private order-participant-route "/api/orders/%s/participant")
(def ^:private order-restaurant-route "/api/orders/%s/restaurant")
(def ^:private order-status-route "/api/orders/%s/executed")
(def ^:private order-items-route "/api/orders/%s/items")
(def ^:private menu-item-route "/api/menu-items/%s")
(def ^:private add-menu-item-route "/api/menu-items")

(defn create-order [name freeze-time]
  (POST create-order-route {:params          {"name"        name
                                              "freeze-time" freeze-time}
                            :format          :json
                            :response-format :json}))

(defn update-order-restaurant [id]
  (PUT (format order-restaurant-route id)))

(defn participant? [id]
  (GET (format order-participant-route id) {:handler (fn [] )}))

(defn update-order-status [id]
  (POST (format order-status-route id)))

(defn add-order-item [id]
  (POST (format order-items-route id)))

(defn add-participant [id]
  (POST (format order-participant-route id)))

(defn get-menu-item [id]
  (GET (format menu-item-route id)))

(defn edit-menu-item [id]
  (PUT (format menu-item-route id)))

(defn delete-menu-item [id]
  (DELETE (format menu-item-route id)))

(defn add-menu-item [name description price restaurant-id]
  (POST add-menu-item-route {:params
                                              {:name          name
                                               :price         price
                                               :description   description
                                               :restaurant-id restaurant-id}
                             :format          :json
                             :response-format :json}))