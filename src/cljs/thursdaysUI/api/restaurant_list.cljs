(ns thursdaysUI.api.restaurant-list
  (:require [thursdaysUI.store :as store]
            [ajax.core :refer [GET POST]]))

(defn update-list []
  (GET "/api/restaurants" {:handler         #(store/add-restaurants %)
                           :response-format :json
                           :keywords?       true}))

(defn create-restaurant [name]
  (POST "/api/restaurants" {:params          {:name name}
                            :handler         (fn [_] (update-list))
                            :format          :json
                            :response-format :json}))
