(ns thursdaysUI.api.order
  (:require [ajax.core :refer [GET POST PUT DELETE]]
            [thursdaysUI.store :as store]
            [thursdaysUI.underscore-converter :refer [correct-key]]))

(enable-console-print!)

(defn order-participant-route [id] (str "/api/orders/" id "/participant"))
(defn order-restaurant-route [id] (str "/api/orders/" id "/restaurant"))
(defn order-items-route [id] (str "/api/orders/" id "/items"))

(defn get [id]
  (GET (str "/api/orders/" id)
       {:handler         #(store/add-order %)
        :response-format :json
        :keywords?       true}))

(defn participate [id]
  (POST (order-participant-route id)
        {:handler #(store/change-participation-state id true)}))

(defn update-order-restaurant [id restaurant-id]
  (POST (order-restaurant-route id) {:params {:restaurant-id restaurant-id}
                                     :format :json}))

(defn update-order-restaurant [id restaurant-id]
  (POST (order-restaurant-route id) {:params {:restaurant-id restaurant-id}
                                     :format :json}))

(defn add-order-item [id [menu-item-id quantity]]
  (POST (order-items-route id)
        {:params {:menuitem-id menu-item-id
                  :quantity    quantity}
         :format :json}))