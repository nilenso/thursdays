(ns thursdaysUI.api.user
  (:require [thursdaysUI.store :as store]
            [ajax.core :refer [GET POST]]))

(def ^:private user-route "/api/user")

(defn get []
  (GET user-route {:response-format :json
                   :keywords?       true
                   :handler         (fn [res]
                                      (store/add-user res))}))
