(ns thursdaysUI.api.menu-items
  (:require [thursdaysUI.store :as store]
            [ajax.core :refer [GET POST PUT DELETE]]
            [thursdaysUI.underscore-converter :refer [correct-key]]))

(enable-console-print!)



(defn get-menu-items [restaurant-id]
  (GET (str "/api/menu-items/" restaurant-id)
       {:handler         #(swap! store/state assoc :menu-items (correct-key %))
        :response-format :json
        :keywords?       true}))

(defn fetch-menu-items [restaurant-id]
  (GET (str "/api/menu-items/" restaurant-id)
       {:handler         #(store/update-menu-items restaurant-id %)
        :response-format :json
        :keywords?       true}))

(defn create-menu-item [{:keys [restaurant-id] :as menu-item}]
  (POST "/api/menu-items"
        {:body            (.stringify js/JSON (clj->js menu-item))
         :handler         (fn [_] (get-menu-items (str restaurant-id)))
         :headers         {:content-type "application/json"}
         :format          :json
         :response-format :json
         :keywords?       true}))

(defn edit-menu-item [{:keys [restaurant-id id] :as menu-item}]
  (let [new-menu-item (dissoc menu-item :id)]
    (PUT (str "/api/menu-items/" id)
         {:body            (.stringify js/JSON (clj->js new-menu-item))
          :handler         (fn [_] (get-menu-items (str restaurant-id)))
          :headers         {:content-type "application/json"}
          :format          :json
          :response-format :json
          :keywords        true})))

(defn delete-menu-item [{:keys [restaurant-id id] :as menu-items}]
  (DELETE (str "/api/menu-items/" id)
          {:handler (fn [_] (get-menu-items restaurant-id))}
          :headers {:content-type "application/json"}
          :format          :json
          :response-format :json
          :keywords?       true))




