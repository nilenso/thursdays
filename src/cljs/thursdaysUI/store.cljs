(ns thursdaysUI.store
  (:require [reagent.core :as r]))

(defonce state (r/atom nil))

(def restaurants-state (r/cursor state [:restaurants]))

(defn menu-state []
  (r/cursor state [:menu-items]))

(defn menu-items-state [restaurant-id]
  (r/cursor state [:restaurant-id->menu-items restaurant-id]))

(defn update-menu-items [restaurant-id menu-items]
  (swap! state assoc-in [:restaurant-id->menu-items restaurant-id] menu-items))

(defn participation-state [order-id]
  (r/cursor state [:orders order-id :participating]))

(defn user-state []
  (r/cursor state [:user]))

(defn order-state [order-id]
  (r/cursor state [:orders order-id]))

(defn add-order
  [{:keys [id] :as order}]
  (swap! state assoc-in [:orders id] order))

(defn add-restaurants
  [restaurants]
  (swap! state assoc :restaurants (into {} (map (juxt :id identity) restaurants))))

(defn add-user [user]
  (swap! state assoc :user user))

(defn change-participation-state [order-id is-participant]
  (swap! state assoc-in [:orders order-id :participating] is-participant))

(defn get-restaurant [restaurant-id]
  (get @restaurants-state restaurant-id))

(defn init []
  (reset! state {:restaurants               {}
                 :restaurant-id->menu-items {}
                 :active-page               :restaurant-list
                 :menu-items                []
                 :user                      {}
                 :orders                    {}}))