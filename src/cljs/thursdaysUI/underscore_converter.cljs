(ns thursdaysUI.underscore-converter)


(defn- replace-underscore [map-entry]
  (let [map-key (name (first map-entry))
        map-value (second map-entry)]
    (vector (keyword (clojure.string/replace map-key #"_" "-")) map-value)))


(defn underscore-to-hyphen-converter [incoming-map]
  (into {} (map replace-underscore incoming-map)))

(defn correct-key [item-list]
  (map underscore-to-hyphen-converter item-list))