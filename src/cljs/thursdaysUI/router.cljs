(ns thursdaysUI.router
  (:require [pushy.core :as pushy]
            [thursdaysUI.store :as store]
            [goog.events]
            [secretary.core :as secretary :refer-macros [defroute]]))


(defn change-active-page [active-page params]
  (swap! store/state
         (fn [m]
           (js/console.log "changing page to " active-page)
           (assoc m :active-page active-page
                    :params params))))

(defroute "/create-order" []
          (change-active-page :create-order {}))

(defroute "/show-order/:id" [id]
          (change-active-page :show-order {:id id}))

(defroute "/menu-items/:id/:name" [id name]
          (change-active-page :restaurant {:id id :name name}))

(defroute "/" []
          (change-active-page :home {}))

(defroute "/back-office" []
          (change-active-page :restaurant-list {}))

(defroute "*" []
          (change-active-page :not-found {}))

(def history (pushy/pushy secretary/dispatch!
                          (fn [route]
                            (secretary/locate-route route) route)))

(defn init []
  (pushy/start! history))
