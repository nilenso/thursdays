(ns thursdays.factories
  (:require [clojure.test :refer :all]
            [thursdays.config :as config]
            [thursdays.db.users :as users-db]
            [thursdays.models.orders :as orders-models]
            [thursdays.models.restaurants :as restaurant-models])
  (:import (java.util UUID)))

(defn get-new-restaurant-id []
  (-> {:name "Some name"}
      (restaurant-models/create-restaurant (config/test-db-spec))
      first
      :id))

(defn create-new-user []
  (-> {:name  "name"
       :email (.toString (UUID/randomUUID))}
      (users-db/create-user (config/test-db-spec))
      first
      :id))

(defn create-new-order
  ([]
   (orders-models/create-order {:name          "Thursdays"
                                :freeze-time   "2020-07-07T06:53:00"
                                :creator-id    (create-new-user)
                                :executed      false
                                :restaurant-id (get-new-restaurant-id)}
                               (config/test-db-spec)))
  ([freeze-time executed]
   (orders-models/create-order {:name          "Thursdays"
                                :freeze-time   freeze-time
                                :creator-id    (create-new-user)
                                :executed      executed
                                :restaurant-id (get-new-restaurant-id)}
                               (config/test-db-spec))))

(defn create-menu-item [name restaurant-id]
  (let [menu-item {:name          name
                   :description   "Not a Pizza"
                   :price         45.00
                   :restaurant-id restaurant-id}]
    (thursdays.models.menu-items/create-menu-item menu-item (config/test-db-spec))))
