(ns thursdays.unit.order-items-test
  (:require [clojure.test :refer :all]
            [thursdays.models.order-items :as order-items]))

(deftest index-order-items-by-user-id
  (testing "Should return a map of user-ids to order-items"
    (let [order-items [{:order-id 4 :ordered-by 54}
                       {:order-id 4 :ordered-by 55 :menuitem-id 1}
                       {:order-id 4 :ordered-by 55 :menuitem-id 2}]]
      (is (= (order-items/index-by-user-id order-items)
             {54 [{:order-id 4 :ordered-by 54}]
              55 [{:order-id 4 :ordered-by 55 :menuitem-id 1}
                  {:order-id 4 :ordered-by 55 :menuitem-id 2}]})))))

