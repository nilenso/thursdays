(ns thursdays.unit.menu-items-test
  (:require [clojure.test :refer :all]
            [thursdays.models.menu-items :as models]
            [thursdays.db.menu_items :refer [get-menu-items-menu-id]]
            [clojure.tools.logging :as log]
            [thursdays.config :as config]
            [thursdays.models.restaurants :refer [create-restaurant]]
            [thursdays.db.users :as users-db]))

(def not-nil? (complement nil?))

;-----
;Success test for Menu-items
;-----
(def valid-restaurant {:name "syhegiouyrbilu"})
(def db-spec (config/test-db-spec))

(defn- get-new-restaurant-id []
  (:id (first (create-restaurant valid-restaurant db-spec))))

(def menu-item {:name          "Burger"
                :description   "Not a Pizza"
                :price         45.00
                :restaurant-id (get-new-restaurant-id)})

(deftest create-menu-item-test
  (testing "create menu item in the database when a valid data is passed"
    (let [[{:keys [id] :as result}] (models/create-menu-item menu-item db-spec)]
      (is (= (get-menu-items-menu-id id db-spec)
             result)))))


;-----
;Failure test for Menu-items
;-----

(def invalid-menu-item {:name          "Burger"
                        :description   "Not a Pizza"
                        :price         "its a string"
                        :restaurant-id (get-new-restaurant-id)})

(deftest create-menu-item-failure-test
  (testing "not create menu item in the database when a invalid data is passed"
    (let [[{:keys [id] :as result}] (try (models/create-menu-item invalid-menu-item db-spec)
                                       (catch Exception e(.getMessage e)))]
      (is (nil? (get-menu-items-menu-id id db-spec))))))


;-----
;Success test for update Menu-items
;-----

(def update-menu-item-data {:name          "Not a Burger"
                            :description   "Maybe badam milk"
                            :price         55.00
                            :restaurant-id (get-new-restaurant-id)})

(deftest update-menu-item-test
  (testing "update existing menu item in the database when valid data is passed"
    (let [[{:keys [id]}] (models/create-menu-item menu-item db-spec)
          [updated-menu-item] (models/edit-menu-item update-menu-item-data (str id) db-spec)]
      (is (= (get-menu-items-menu-id id db-spec)
             updated-menu-item)))))

;-----
;Failure test for update Menu-items
;-----
(def update-invalid-menu-item-data {:name          "Not a Burger"
                            :description   "Maybe badam milk"
                            :price         "nope not telling"
                            :restaurant-id (get-new-restaurant-id)})

(deftest update-menu-item-test
  (testing "not update existing menu item in the database when invalid data is passed"
    (let [[{:keys [id] :as result}] (models/create-menu-item menu-item db-spec)]
      (try (models/edit-menu-item update-invalid-menu-item-data
                                  (str id)
                                  db-spec)
           (catch Exception e (.getMessage e)))
      (is (= (get-menu-items-menu-id id db-spec)
             result)))))

;------
;success test for deleting menu-items
;------
(def delete-menu-item-data {:name          "Not a Burger"
                            :description   "Maybe badam milk"
                            :price         55.00
                            :restaurant-id (get-new-restaurant-id)})
(deftest delete-menu-item-test
  (testing "delete an existing item in the database when valid id is passed"
    (let [[{:keys [id]}] (models/create-menu-item delete-menu-item-data db-spec)]
      (models/delete-menu-item (str id) db-spec)
      (is (nil? (get-menu-items-menu-id id db-spec))))))


;------
;Failure test for deleting menu-items
;------
(def invalid-delete-menu-item-data {:name          "Not a Burger"
                            :description   "Maybe badam milk"
                            :price         55.00
                            :restaurant-id (get-new-restaurant-id)})
(deftest delete-menu-item-test
  (testing "does not delete an existing item in the database when invalid id is passed"
    (let [[{:keys [id]}] (models/create-menu-item invalid-delete-menu-item-data db-spec)]
      (try (models/delete-menu-item (str (inc id)) db-spec)
           (catch Exception e (.getMessage e)))
      (is (not-nil? (get-menu-items-menu-id id db-spec))))))








