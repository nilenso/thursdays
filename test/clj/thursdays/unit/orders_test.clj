(ns thursdays.unit.orders-test
  (:require [clojure.test :refer :all]
            [thursdays.models.orders :as orders-models]
            [thursdays.db.orders :as orders-db]
            [clojure.tools.logging :as log]
            [thursdays.factories :refer [create-new-user create-new-order get-new-restaurant-id]]
            [thursdays.config :as config]))

(def not-nil? (complement nil?))

(defn rand-long [n]
  (long (rand n)))

(defn- create-new-order-invalid []
  (try (orders-models/create-order {:name          "Thursdays"
                                    :freeze-time   "123"
                                    :creator-id    1
                                    :executed      "gibberish"
                                    :restaurant-id (get-new-restaurant-id)} (config/test-db-spec))
       (catch Exception e (.getMessage e))))

;-------
; Test for create orders
;-------

(deftest create-order-test
  (testing "creates an order row when valid json is passed"
    (let [query-result (create-new-order)
          id (:id (first query-result))]
      (is (not-nil? id)))))

(deftest create-order-test-failure
  (testing "fails to create an order row when invalid json is passed"
    (let [query-result (create-new-order-invalid)
          id (:id (first query-result))]
      (is (nil? id)))))

;-------
; Test for update restaurants in orders
;-------

(deftest update-order-restaurant-test
  (testing "updates the restaurant value given a valid order-id"
    (let [query-result (create-new-order)
          order-id (:id (first query-result))
          restaurant-id (get-new-restaurant-id)
          update-query (orders-models/update-order-restaurant {:order-id      order-id
                                                               :restaurant-id restaurant-id} (config/test-db-spec))
          updated-restaurant-id (:restaurant_id (first update-query))]
      (is (= updated-restaurant-id restaurant-id)))))

(deftest update-order-restaurant-test-failure
  (testing "fails to update the order with restaurant value when invalid json is passed")
  (let [query-result (create-new-order)
        order-id (:id (first query-result))
        restaurant-id (rand-long 9999999)
        update-query (orders-models/update-order-restaurant {:order-id      order-id
                                                             :restaurant-id restaurant-id} (config/test-db-spec))
        updated-restaurant-id (:restaurant_id (first update-query))]
    (is (not= updated-restaurant-id restaurant-id))))

;-------
; Test for update executed in orders
;-------
(deftest update-order-executed-test
  (testing "updates the executed value given a valid order-id"
    (let [query-result (create-new-order)
          order-id (:id (first query-result))
          user-id (:creator_id (first query-result))
          executed true
          update-query (try (orders-models/update-order-executed
                              {:order-id order-id
                               :user-id  user-id
                               :executed executed} (config/test-db-spec))
                            (catch Exception e (.getMessage e)))
          updated-executed-value (:executed (first update-query))]

      (log/info "order-id" order-id)
      (log/info "update-query" update-query)
      (is (= updated-executed-value executed)))))

(deftest update-order-executed-test-failure
  (testing "updates the executed value given a valid order-id"
    (let [query-result (create-new-order)
          order-id (:id (first query-result))
          user-id (:creator_id (first query-result))
          executed "false"
          update-query (try (orders-models/update-order-executed
                              {:order-id order-id
                               :user-id  user-id
                               :executed executed} (config/test-db-spec))
                            (catch Exception e (.getMessage e)))
          updated-executed-value (:executed (first update-query))]

      (log/info "order-id" order-id)
      (log/info "update-query" update-query)
      (is (not= updated-executed-value executed)))))



