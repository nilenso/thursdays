(ns thursdays.unit.participants-test
  (:require [clojure.test :refer :all]
            [thursdays.unit.orders-test :as orders-unit-test]
            [thursdays.models.participants :as participants-model]
            [thursdays.config :as config]
            [clojure.tools.logging :as log]
            [thursdays.factories :as factories]))

(def not-nil? (complement nil?))

(defn- create-participant [order-id user-id]
  (try (participants-model/create-participant {:order-id order-id
                                               :user-id  user-id} (config/test-db-spec))
       (catch Exception e (.getMessage e))))

(deftest create-participant-test
  (testing "Creates a participant row when the order-id, user-id pair doesn't exist"
    (let [order-id (:id (first (factories/create-new-order)))
          user-id (factories/create-new-user)
          participant (create-participant order-id user-id)
          participant-id (:id (first participant))]
      (is (not-nil? participant-id)))))

(deftest create-participant-test-failure
  (testing "Fails to create a participant row when the order-id, user-id pair exists"
    (let [order-id (:id (first (factories/create-new-order)))
          user-id (factories/create-new-user)
          participant (create-participant order-id user-id)
          participant-id (:id (first participant))
          participant-again (create-participant order-id user-id)
          participant-again-id (:id (first participant-again))]
      (is (not-nil? participant-id))
      (is (nil? participant-again-id)))))



