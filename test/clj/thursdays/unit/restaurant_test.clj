(ns thursdays.unit.restaurant-test
  (:require [clojure.test :refer :all]
            [thursdays.db.restaurants :refer [get-restaurant]]
            [thursdays.config :as config]
            [thursdays.models.restaurants :refer [create-restaurant]]))

(def not-nil? (complement nil?))
(def valid-restaurant {:name "syhegiouyrbilu"})
(def invalid-restaurant {:name 20/564564})
(def db-spec (config/test-db-spec))

;-----
;Success test for create restaurant
;-----
(deftest create-restaurant-test
  (testing "creates a restaurant when valid data is passed"
    (let [{:keys [id] :as result} (first (create-restaurant valid-restaurant db-spec))]
      (is (= (get-restaurant id db-spec))
          result))))

;-----
;Failure test for create restaurant
;-----
(deftest create-restaurant-test
  (testing "creates a restaurant when valid data is passed"
    (let [{:keys [id] :as result} (first (try (create-restaurant valid-restaurant db-spec)
                                              (catch Exception e (.getMessage e))))]
      (is (= (get-restaurant id db-spec))
          result))))