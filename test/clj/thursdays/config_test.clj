(ns thursdays.config-test
  (:require [clojure.test :refer :all]
            [thursdays.db.migrations :as migrations]
            [thursdays.http.server :as server]
            [thursdays.config :as config]
            [clojure.java.jdbc :as jdbc]
            [clojure.tools.logging :as log]))

(defn database-setup [f]
  (migrations/rollback-all-test)
  (migrations/migrate-test)
  (prn "Cleared and migrated DB")
  (f)
  (prn "Cleared DB"))

(defn test-port [] 9002)

(defn server-setup [f]
  (server/start (str (test-port)))
  (f)
  (server/stop))

(defn transaction-setup [f]
  (do (jdbc/with-db-transaction [trans-conn (config/db-spec)]
                                (jdbc/db-set-rollback-only! trans-conn)
                                (log/info "DB SPEC" (config/db-spec))
                                (log/info "TRANSACTION" trans-conn)))
  (f))

;(defn transaction-setup [f]
;  (log/info "before deref" (config/db-spec))
;  (jdbc/with-db-transaction [t-con (config/db-spec)]
;                            (log/info "TCON" t-con)
;                            (jdbc/db-set-rollback-only! t-con)
;                            (with-redefs [config/db-spec t-con
;                                                    config/foo 44]
;                              (f)
;                              (log/info "somelohhg" (jdbc/insert! config/db-spec :restaurants {:name "kfc"})))))


;(with-redefs [a (constantly 1)]
;  ([a []]))
;
;(with-redefs [type (constantly 1)
;              class (constantly 10)]
;  [(type [])
;   (class [])])
;

;(with-redefs [foo ] foo)

;(prn (config/db-spec))

;(jdbc/with-db-transaction [t-con (config/db-spec)]
;                          (jdbc/db-set-rollback-only! t-con)
;                          (jdbc/insert! t-con :restaurants "kfc"))