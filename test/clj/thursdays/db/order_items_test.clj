(ns thursdays.db.order-items-test
  (:require [clojure.test :refer :all]
            [thursdays.db.order-items :as order-items]
            [thursdays.factories :as factories]
            [thursdays.config :as config]))

(deftest get-all-order-items
  (testing "Should return all order items created for an order"
    (let [{:keys [id restaurant-id creator-id] :as order} (first (factories/create-new-order))
          user-id (factories/create-new-user)
          {menu-item-id :id} (first (factories/create-menu-item "P" restaurant-id))
          {menu-item2-id :id} (first (factories/create-menu-item "Q" restaurant-id))]
      (prn "XYZ" {:order_id    id
                  :quantity    2
                  :menuitem_id menu-item-id
                  :ordered_by  creator-id})
      (order-items/create-order-item {:order_id    id
                                      :quantity    2
                                      :menuitem_id menu-item-id
                                      :ordered_by  creator-id
                                      :ordered_for nil}
                                     (config/test-db-spec))
      (order-items/create-order-item {:order_id    id
                                      :quantity    1
                                      :menuitem_id menu-item2-id
                                      :ordered_by  creator-id
                                      :ordered_for nil}
                                     (config/test-db-spec))
      (order-items/create-order-item {:order_id    id
                                      :quantity    2
                                      :menuitem_id menu-item2-id
                                      :ordered_by  user-id
                                      :ordered_for nil}
                                     (config/test-db-spec))

      (is (= #{{:order_id    id
                :quantity    2
                :menuitem_id menu-item-id
                :ordered_by  creator-id
                :ordered_for nil}
               {:order_id    id
                :quantity    1
                :menuitem_id menu-item2-id
                :ordered_by  creator-id
                :ordered_for nil}
               {:order_id    id
                :quantity    2
                :menuitem_id menu-item2-id
                :ordered_by  user-id
                :ordered_for nil}}
             (->> (order-items/get-all id (config/test-db-spec))
                    (map #(dissoc % :id))
                  (into #{})))))))
