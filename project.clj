(defproject thursdays "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "APACHE-2.0 WITH Classpath-exception-2.0"
            :url  "https://www.apache.org/licenses/LICENSE-2.0"}
  :min-lein-version "2.9.1"


  :dependencies [[org.clojure/clojure "1.10.0"]
                 [aero "1.1.3"]
                 [ragtime "0.8.0"]
                 [org.clojure/tools.logging "0.4.1"]
                 [ring "1.7.1" :exclusions [ring/ring-codec clj-time commons-codec joda-time]]
                 ;[ragtime "0.8.0"]
                 [compojure "1.6.1"]
                 [org.clojure/java.jdbc "0.7.9"]
                 [honeysql "0.9.4"]
                 [com.google.oauth-client/google-oauth-client "1.30.1" :exclusions [org.apache.httpcomponents/httpcore commons-codec]]
                 [ring-oauth2 "0.1.4"]
                 [ring/ring-defaults "0.3.2"]
                 [org.postgresql/postgresql "42.2.5"]
                 [org.clojure/algo.generic "0.1.3"]
                 [org.clojure/tools.logging "0.4.1"]
                 [ring/ring-json "0.4.0"]
                 [nilenso/honeysql-postgres "0.2.6"]
                 [org.clojure/core.async "0.4.500"]
                 [clj-http "3.10.0"]
                 [cheshire "5.8.1"]]

  :aliases {"migrate" ["run" "-m" "thursdays.db.migrations/migrate"]
            "migrate-test" ["run" "-m" "thursdays.db.migrations/migrate-test"]
            "rollback" ["run" "-m" "thursdays.db.migrations/rollback"]
            "rollback-all-test" ["run" "-m" "thursdays.db.migrations/rollback-all-test"]}
  :main ^:skip-aot thursdays.core
  :target-path "target/%s"
  :source-paths ["src/clj" "src/cljs"]
  :test-paths ["test/clj" "src/cljc" "test/cljs"]


  :profiles {:uberjar {:aot :all}
             :dev     {:source-paths   ["src/clj" "src/cljc" "src/cljs"]
                       :resource-paths ["resources/clj" "src/cljc" "resources/cljs"]
                       :test-paths     ["test/clj" "src/cljc" "test/cljs"]
                       :dependencies   [[org.clojure/clojure "1.10.0"]
                                        [org.clojure/clojurescript "1.10.520"]
                                        [reagent "0.8.1"]
                                        [cljs-http "0.1.46" :exclusions [commons-codec]]
                                        [org.clojure/algo.generic "0.1.3"]
                                        [cljs-ajax "0.7.5"]
                                        [clj-commons/secretary "1.2.4"]
                                        [kibu/pushy "0.3.8"]]

                       :plugins        [[lein-figwheel "0.5.19"]
                                        [lein-cljsbuild "1.1.7" :exclusions [[org.clojure/clojure]]]]

                       :cljsbuild      {:builds
                                        [{:id           "-A:dev"
                                          :source-paths ["src/cljs"]
                                          :figwheel     {:on-jsload "thursdaysUI.core/on-js-reload"}

                                          :compiler     {:main                 thursdaysUI.core
                                                         :asset-path           "/js/compiled/out"
                                                         :output-to            "resources/cljs/public/js/compiled/thursdaysUI.js"
                                                         :output-dir           "resources/cljs/public/js/compiled/out"
                                                         :source-map-timestamp true

                                                         :preloads             [devtools.preload]}}
                                         {:id           "min"
                                          :source-paths ["src/cljs"]
                                          :compiler     {:output-to     "resources/cljs/public/js/compiled/thursdaysUI.js"
                                                         :main          thursdaysUI.core
                                                         :optimizations :advanced
                                                         :pretty-print  false}}]}
                       :figwheel       {:css-dirs ["resources/cljs/public/css"]
                                        :server-port 8001}
                       }})
