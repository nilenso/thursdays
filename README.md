# Thursdays

## Running Thursdays
- Using the following command applies the migrations and starts the server on port 9001 in localhost
`lein run`

## Running docker compose
 - Using the documentation [here](https://docs.docker.com/compose/install/) install docker and docker compose
 - Run compose by using the following command in the project directory 
  `docker-compose up ` 
 - Run compose in the backgroud by using the following command in the project directory 
  `docker-compose up -d` 
- Connect to the psql database using the following command in the project directory
`psql -d thursdays_db -U thursdays_dev -p 7001 -h localhost`
- When prompted put **pwd** as password
- To list all currently running containers:
 `docker-compose ps`
- To take down the container:
`docker-compose down`

