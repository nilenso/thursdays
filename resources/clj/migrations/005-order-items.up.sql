CREATE TABLE order_items (id SERIAL PRIMARY KEY,
    menuitem_id INTEGER NOT NULL REFERENCES menu_items (id),
    quantity INTEGER NOT NULL,
    ordered_by INTEGER REFERENCES users (id),
    ordered_for INTEGER REFERENCES users (id) DEFAULT NULL,
    order_id INTEGER REFERENCES orders (id));