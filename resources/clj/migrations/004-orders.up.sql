
CREATE TABLE orders ( id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    freeze_time TIMESTAMP,
    creator_id INTEGER REFERENCES users (id),
    executed BOOLEAN DEFAULT FALSE,
    restaurant_id INTEGER REFERENCES restaurants (id));