CREATE TABLE menu_items (id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    description TEXT DEFAULT NULL,
    price NUMERIC(9,2) NOT NULL,
    restaurant_id INTEGER REFERENCES restaurants(id));