CREATE TABLE participants (id SERIAL PRIMARY KEY,
    order_id INTEGER REFERENCES orders (id) NOT NULL,
    user_id INTEGER REFERENCES users (id) NOT NULL,
     UNIQUE (order_id, user_id)) ;