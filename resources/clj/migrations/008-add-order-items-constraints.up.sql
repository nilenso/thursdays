ALTER TABLE order_items ADD constraint unique_order_id_menuitem_id_ordered_by_ordered_for UNIQUE(ordered_by, ordered_for, order_id, menuitem_id);
